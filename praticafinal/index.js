const express = require('express')
const request = require('request')

let bodyParser = require('body-parser');
let app = express();

var api = "https://f5zg6v0z92.execute-api.us-east-1.amazonaws.com/dev"

app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())  ;

// Listar todos na pagina principal
app.get('/', (req, res) => { 
    request({   url: api + "/contacts", 
                method: "GET"      
            }, function(error, response, body) {
                if(error){
                    console.log(error)
                } else {
                    if(response.statusCode == 200){
                        var bodyjson = JSON.parse(body)
                        var data = bodyjson
                        console.log(data)
                        res.render('index.ejs', { data })
                    } 
                }
            });
})

// Abre a view para registrar um contato
app.get('/add-contact',(req, res) => {
    res.render('add-contact.ejs')   
})

// Registra um contato na api
app.post('/add-contact',(req, res)=>{
    
    var name = req.body.name
    var gender = req.body.gender
    var email = req.body.email
    var phone = req.body.phone
    
	var contact = {
		"name": name,
        "gender": gender,
        "email": email,
        "phone": phone
	}
	request({
		url: api + "/contacts",
		method: "POST",
		json: contact},
		function(err,response,body){
                console.log(response.statusCode);
                res.redirect('/')
		});
})

//Atualiza um contato
app.get('/update-contact/:id',(req, res)=>{
    var id = req.params.id

    request({
            url: api +"/contacts/" + id,
            method: "GET"},function(err,response,body){
                if(err){
                    console.log(err)
                }else{
                    if(response.statusCode == 200){
                        var bodyjson = JSON.parse(body)
                        var data = bodyjson
                        res.render('update-contact.ejs', { data })
                    }
                }
            }
    )
    	
});

app.post('/update-contact',(req, res)=>{
    var umid = req.body.umid

    var name = req.body.name
    var gender = req.body.gender
    var email = req.body.email
    var phone = req.body.phone
    
	var contact = {
		"name": name,
        "gender": gender,
        "email": email,
        "phone": phone
    }
    request({
            url: api +"/contacts/" + umid,
            json: contact,
            method: "PUT"},function(err,response,body){
                if(err){
                    console.log(err)
                }else{
                    if(response.statusCode == 200){
                        console.log(body)
                        var data = body
                        res.render('update-contact.ejs', { data })
                    }
                }
            }
    )
    	
});

//Busca um contato
app.get('/search-contact',(req, res) => {
    res.render('search-contact.ejs')   
})

app.post('/search-contact', (req, res) => {
    var esseid = req.body.esseid
    console.log(esseid)
    request({
        url : api +"/contacts/" + esseid,
        method: "GET"},function(err,response,body){
            if(err){
                console.log(err)
            }else{
                if(response.statusCode == 200){
                    var data = []
                    var bodyjson = JSON.parse(body)
                    data.push(bodyjson)
                    res.render('index.ejs', { data })
                }
            }
        })
})

//Delete contatos
app.get('/delete/:id', function (req, res) {

    var id = req.params.id

    request({   
                url: api + "/contacts/" + id,
                method: "DELETE"
            },
            function(err,response,body){
                console.log(response.statusCode);
                res.redirect('/')
            }
    )

});

app.listen(8888, function () {
	console.log('Example app listening on port 8888!');
});