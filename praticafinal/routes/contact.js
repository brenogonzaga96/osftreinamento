const router = require('express').Router();

const controllerContact = require('../controller/contact');

router.get('/', contactController.list);
router.post('/add', contactController.save);
router.get('/update/:id', contactController.edit);
router.post('/update/:id', contactController.update);
router.get('/delete/:id', contactController.delete);

module.exports = router;

